import { IssueMapProps } from "./IssueMapProps";

import {
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
  Tooltip,
} from "../ui/tooltip";

import {
  statusBorderColors,
  statusBackgroundColorsIssueMap as statusBackgroundColors,
} from "@/utils/colors";

export const IssueMap: React.FC<IssueMapProps> = ({
  issues,
  blueprintUrl,
  onAdd,
  onView,
  isAddingIssue = false,
  newIssue,
}) => {
  return (
    <div className="overflow-x-auto">
      <div
        className={`relative w-max ${isAddingIssue ? "cursor-crosshair" : ""}`}
      >
        <img
          src={blueprintUrl}
          alt="Blueprint"
          onClick={onAdd}
          className="max-h-[70dvh]"
        />
        {issues.length > 0 &&
          issues.map((issue) => (
            <TooltipProvider key={issue.id} delayDuration={150}>
              <Tooltip>
                <TooltipTrigger
                  className="absolute"
                  style={{
                    left: `${issue.coordX}%`,
                    top: `${issue.coordY}%`,
                  }} // tailwind doesn't accept % out of the box
                >
                  <div
                    onClick={() => onView && onView(issue)}
                    className={`size-5 rounded-full border-4 saturate-200 ${onView ? "cursor-pointer" : ""} ${statusBorderColors[issue.statusId]}  ${statusBackgroundColors[issue.statusId]}`}
                  />
                </TooltipTrigger>
                <TooltipContent>
                  <p>{issue.title}</p>
                </TooltipContent>
              </Tooltip>
            </TooltipProvider>
          ))}
        {newIssue && (
          <div
            className="absolute size-6 rounded-full border-4 border-dashed border-red-500"
            style={{
              left: `${newIssue.coordX - 0.15}%`,
              top: `${newIssue.coordY - 0.3}%`,
            }} // tailwind doesn't accept % out of the box
          />
        )}
      </div>
    </div>
  );
};
