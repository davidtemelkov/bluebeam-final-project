import { Role } from "@/utils/enums/role";
import React, { ReactNode, createContext, useEffect, useState } from "react";

interface User {
  name: string | undefined;
  role: Role.MANAGER | Role.WORKER | undefined;
  id: number | undefined;
}

interface UserContextProps {
  user: User | null;
  setUser: (user: User) => void;
  isManager: () => boolean;
}

const UserContext = createContext<UserContextProps>({
  user: {
    name: undefined,
    role: undefined,
    id: undefined,
  },
  setUser: () => null,
  isManager: () => false,
});

const UserProvider: React.FC<{ children: ReactNode }> = ({ children }) => {
  const [user, setUser] = useState<User | null>(null);
  const isManager = () => {
    return !!user && user.role == Role.MANAGER;
  };

  useEffect(() => {
    const savedUser = localStorage.getItem("user");
    if (savedUser) {
      setUser(JSON.parse(savedUser));
    }
  }, []);

  useEffect(() => {
    if (user) {
      localStorage.setItem("user", JSON.stringify(user));
    }
  }, [user]);

  return (
    <UserContext.Provider value={{ user, setUser, isManager }}>
      {children}
    </UserContext.Provider>
  );
};

export { UserContext, UserProvider };

