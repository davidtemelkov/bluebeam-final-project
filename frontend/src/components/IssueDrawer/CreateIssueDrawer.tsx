import { useContext, useState } from "react";
import { toast } from "sonner";

import { IssueLocation, IssueResponse } from "@/services/issue.service";
import { Status } from "@/utils/enums/status";
import {
  validateDescription,
  validateFile,
  validateName,
} from "@/utils/validations";
import { Button } from "../ui/button";
import { Input } from "../ui/input";
import { Label } from "../ui/label";
import {
  Sheet,
  SheetClose,
  SheetContent,
  SheetFooter,
  SheetHeader,
  SheetTitle,
} from "../ui/sheet";

import users from "@/data/users.json";
import { IssueBody, create } from "@/services/issue.service";
import { getErrorMessage } from "@/utils/getErrorMessage";
import { FormError } from "../App/FormError";
import { UserContext } from "../Context/UserContext";
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "../ui/select";
import { Textarea } from "../ui/textarea";

interface IIssueDrawerProps {
  setIssues: React.Dispatch<React.SetStateAction<IssueResponse[]>>;
  facilityId: string;
  spaceId: string | undefined;
  openDrawer: boolean;
  newIssue: IssueLocation | null;
  setOpenDrawer: React.Dispatch<React.SetStateAction<boolean>>;
  setNewIssue: React.Dispatch<React.SetStateAction<IssueLocation | null>>;
}

export const CreateIssueDrawer: React.FC<IIssueDrawerProps> = ({
  setIssues,
  facilityId,
  spaceId,
  openDrawer,
  newIssue,
  setOpenDrawer,
  setNewIssue,
}) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [assignee, setAssignee] = useState<string | undefined>(undefined);
  const [file, setFile] = useState<File | null>(null);
  const [errors, setErrors] = useState({
    title: "",
    description: "",
    file: "",
  });
  const { user, isManager } = useContext(UserContext);

  const handleOpenChange = (openDrawer: boolean) => {
    setOpenDrawer(openDrawer);
    resetForm();
    setErrors({
      title: "",
      description: "",
      file: "",
    });
    setNewIssue(null);
  };

  const formIsValid = () => {
    const titleError = validateName(title);
    const descriptionError = validateDescription(description);
    const fileError = validateFile(file, false);
    setErrors({
      title: titleError,
      description: descriptionError,
      file: fileError,
    });
    return !titleError && !descriptionError && !fileError;
  };

  function handleTitle(event: React.FocusEvent<HTMLInputElement>) {
    const error = validateName(event.currentTarget.value);
    setErrors((prevErrors) => ({ ...prevErrors, title: error }));
  }

  function handleDescription(event: React.FocusEvent<HTMLTextAreaElement>) {
    const error = validateDescription(event.currentTarget.value);
    setErrors((prevErrors) => ({ ...prevErrors, description: error }));
  }

  const handleFileChange = (e: React.FocusEvent<HTMLInputElement>) => {
    if (e.target.files) {
      const fileError = validateFile(e.target.files[0], false);
      setErrors((prevErrors) => ({ ...prevErrors, file: fileError }));
      setFile(e.target.files[0]);
    }
  };

  const resetForm = () => {
    setTitle("");
    setDescription("");
    setAssignee(undefined);
    setFile(null);
    setErrors({
      title: "",
      description: "",
      file: "",
    });
  };

  const handleAssignToMe = () => {
    if (user && user.id) {
      setAssignee(user.id.toString());
    }
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    if (!formIsValid()) {
      toast.error("The form is invalid");
      return;
    }
    try {
      const newCreateIssue: Omit<IssueBody, "id"> = {
        title: title,
        assigneeId: assignee ? +assignee : null,
        description: description,
        statusId: Status.TODO,
        createdAt: new Date().toLocaleString("en-GB", {
          dateStyle: "medium",
          timeStyle: "medium",
        }),
        resolvedAt: null,
        spaceId: spaceId!,
        facilityId: facilityId,
        coordX: newIssue!.coordX,
        coordY: newIssue!.coordY,
        file: file,
        space: null,
        facility: null,
      };

      setOpenDrawer(false);

      const issue = await create(newCreateIssue);

      setIssues((prevIssues) => [...prevIssues, issue]);
      toast.success("Issue created successfully");
    } catch (e) {
      toast.error(getErrorMessage(e));
    } finally {
      resetForm();
      setNewIssue(null);
    }
  };

  return (
    <Sheet open={openDrawer} onOpenChange={handleOpenChange}>
      <SheetContent side={"right"}>
        <SheetHeader>
          <SheetTitle>Add new issue</SheetTitle>
        </SheetHeader>

        <div>
          <Label htmlFor="title">Title</Label>
          <Input
            id="title"
            type="text"
            required
            onBlur={handleTitle}
            onChange={(e) => setTitle(e.currentTarget.value)}
            value={title}
          />
          <FormError error={errors.title} />
        </div>

        <div>
          <Label htmlFor="description">Description</Label>
          <Textarea
            id="description"
            className="min-h-[1rem]"
            required
            onBlur={handleDescription}
            onChange={(e) => setDescription(e.currentTarget.value)}
            value={description}
          />
          <FormError error={errors.description} />
        </div>

        <div>
          <label
            className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
            htmlFor="assigneeId"
          >
            Assignee
          </label>
          <Select value={assignee} onValueChange={setAssignee}>
            <SelectTrigger className="h-[30px] w-[170px]">
              <SelectValue placeholder="Select an assignee" />
            </SelectTrigger>
            <SelectContent>
              <SelectGroup>
                {users.map((user) => (
                  <SelectItem value={user.id.toString()} key={user.id}>
                    {user.name}
                  </SelectItem>
                ))}
              </SelectGroup>
            </SelectContent>
          </Select>
          {!isManager() && (
            <p
              onClick={handleAssignToMe}
              className="my-1 cursor-pointer text-sm text-blue-500"
            >
              Assign to me
            </p>
          )}
        </div>

        <div>
          <Label htmlFor="blueprint">Photo of the issue</Label>
          <Input
            id="blueprint"
            type="file"
            className="file:text-secondary-foreground"
            onChange={handleFileChange}
          />
          <FormError error={errors.file} />
        </div>

        <SheetFooter>
          <SheetClose asChild>
            <Button
              className="mt-8 text-white"
              type="submit"
              onClick={handleSubmit}
            >
              Submit
            </Button>
          </SheetClose>
        </SheetFooter>
      </SheetContent>
    </Sheet>
  );
};
