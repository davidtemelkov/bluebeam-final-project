import React from "react";
import { NavigateFunction } from "react-router-dom";

interface IMapboxPopupProps {
  id: string;
  name: string;
  url: string;
  photoUrl: string;
  address: string;
  issuesCount?: number;
  navigate: NavigateFunction;
}

export const MapboxPopup: React.FC<IMapboxPopupProps> = ({
  id,
  name,
  url,
  photoUrl,
  address,
  issuesCount,
  navigate,
}) => {
  return (
    <div key={id} className="group flex flex-col items-center gap-2">
      <h3
        className="cursor-pointer text-lg font-semibold text-blue-500"
        onClick={() => navigate(url)}
      >
        {name}
      </h3>
      <p className="text-center text-black">
        Open issues count:{" "}
        <span
          className={issuesCount && issuesCount > 0 ? `text-destructive` : ""}
        >
          {issuesCount}
        </span>
      </p>
      <img
        src={photoUrl}
        alt={name}
        className="h-36 w-full rounded-[0.6rem] object-cover"
      />
      <p className="text-center text-black">{address}</p>
    </div>
  );
};
