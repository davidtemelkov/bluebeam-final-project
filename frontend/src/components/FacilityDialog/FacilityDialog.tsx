import { useEffect, useState } from "react";

import {
  FacilityBody,
  UpdateFacilityData,
  create,
  update,
} from "@/services/facility.service";
import {
  validateAddress,
  validateFile,
  validateName,
} from "@/utils/validations";
import { IFacilityDialogProps } from "./IFacilityDialogProps";

import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { getErrorMessage } from "@/utils/getErrorMessage";
import { PlusIcon } from "lucide-react";
import { Link } from "react-router-dom";
import { toast } from "sonner";
import { Address, AddressInput } from "../AddressInput";
import { FormError } from "../App/FormError";
import { Input } from "../ui/input";
import { Label } from "../ui/label";

export const CreateFacility: React.FC<IFacilityDialogProps> = ({
  addFacility,
  updateFacility,
  predefinedData,
  onClose,
}) => {
  const [name, setName] = useState("");
  const [address, setAddress] = useState<Address | null>(null);
  const [file, setFile] = useState<File | null>(null);
  const [errors, setErrors] = useState({
    name: "",
    address: "",
    file: "",
  });
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (predefinedData) {
      removeErrors();
      setName(predefinedData?.name || name);
      setAddress({ full_address: predefinedData.address! });
      setOpen(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [predefinedData]);

  const resetForm = () => {
    setName("");
    setAddress(null);
    setFile(null);
  };

  function removeErrors() {
    setErrors({ name: "", address: "", file: "" });
  }

  const formIsValid = () => {
    const nameError = validateName(name);
    const addressError = validateAddress(address, !predefinedData);

    const fileError = validateFile(file, !predefinedData);
    setErrors({ name: nameError, address: addressError, file: fileError });

    return !nameError && !addressError && !fileError;
  };

  const handleNameBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    const error = validateName(e.target.value);
    setErrors((prevErrors) => ({ ...prevErrors, name: error }));
  };

  const handleFileChange = (e: React.FocusEvent<HTMLInputElement>) => {
    if (e.target.files) {
      const file = e.target.files[0];

      const fileError = validateFile(file, !predefinedData);
      setErrors((prevErrors) => ({ ...prevErrors, file: fileError }));
      setFile(file);
    }
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    if (loading || !formIsValid()) {
      toast.error("The form is invalid!");
      return;
    }

    try {
      setLoading(true);
      // if there is predefinedData we have the assumption it's an update view
      setOpen(false);
      const isUpdateView = !!predefinedData;
      if (isUpdateView) {
        const data = {
          id: predefinedData.id,
          file: file || undefined,
          name,
          address: address?.full_address,
        } as UpdateFacilityData;

        if (address?.coordinates?.latitude)
          data.lat = address?.coordinates?.latitude;

        if (address?.coordinates?.longitude)
          data.lng = address?.coordinates?.longitude;

        const result = await update(data);

        if (!result) {
          toast.error("Error updating facility");
          return;
        }

        if (updateFacility) {
          updateFacility({
            id: predefinedData.id,
            address: address?.full_address,
            name,
            photoUrl: result.photoUrl,
          });
        }

        toast.success("Successfully updated facility", {
          duration: 2000,
          action: (
            <Link
              to={`/${encodeURIComponent(predefinedData.id + "~" + name)}/spaces`}
              className="text-blue-500"
            >
              View Spaces
            </Link>
          ),
        });
      } else {
        if (!address || !address.coordinates) {
          console.error("invalid address");
          toast.error("Invalid address provided");
          return;
        }

        const newFacility: Omit<FacilityBody, "id"> = {
          name: name,
          address: address.full_address,
          lat: address.coordinates.latitude,
          lng: address.coordinates.longitude,
          file: file!,
        };
        setOpen(false);

        const facility = await create(newFacility);
        addFacility(facility);

        toast.success("Successfully created facility", {
          duration: 2000,
          action: (
            <Link
              to={`/${encodeURIComponent(facility.id + "~" + facility.name)}/spaces`}
              className="text-blue-500"
            >
              Create Spaces
            </Link>
          ),
        });
      }

      resetForm();
    } catch (e) {
      toast.error(getErrorMessage(e));
    } finally {
      setLoading(false);
    }
  };

  const handleOpenChange = (isOpen: boolean) => {
    setOpen(isOpen);
    resetForm();
    removeErrors();
    if (onClose) {
      onClose();
    }
  };

  return (
    <Dialog open={open} onOpenChange={handleOpenChange}>
      <DialogTrigger asChild>
        <Button variant="ghost">
          <PlusIcon />
        </Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>
            {predefinedData ? "Update" : "Create"} Facility
          </DialogTitle>
        </DialogHeader>

        <div>
          <Label htmlFor="name">Name</Label>
          <Input
            id="name"
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value)}
            onBlur={handleNameBlur}
          />
          <FormError error={errors.name} />
        </div>

        <div>
          <Label htmlFor="address">Address</Label>
          <AddressInput
            initialQuery={predefinedData?.address}
            onSelect={(address) => {
              setAddress(address);
              setErrors((prevErrors) => ({ ...prevErrors, address: "" }));
            }}
          />
          <FormError error={errors.address} />
        </div>

        <div>
          <Label htmlFor="picture">Photo</Label>
          <Input
            id="picture"
            type="file"
            className="file:text-secondary-foreground"
            onChange={handleFileChange}
          />
          <FormError error={errors.file} />
        </div>

        <DialogFooter>
          <Button type="submit" onClick={handleSubmit} className="text-white">
            Submit
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};
