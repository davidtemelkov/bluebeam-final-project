import { cn } from "@/lib/utils";

export type FormErrorProps = {
  error: string | undefined;
  className?: string;
};

export function FormError({ error, className }: FormErrorProps) {
  if (!error || error === "") return <></>;
  return <p className={cn("text-sm text-destructive", className)}>{error}</p>;
}
