import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Trash2Icon } from "lucide-react";

interface IDeleteItemProps {
  onDelete: () => void;
}

export const DeleteItem: React.FC<IDeleteItemProps> = ({ onDelete }) => {
  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button
          variant="outline"
          title="Delete"
          className="hover:border-primary"
          size="icon"
        >
          <Trash2Icon />
        </Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>Are you sure you want to delete this item?</DialogTitle>
        </DialogHeader>
        <p>This action cannot be undone.</p>
        <DialogFooter className="gap-4 sm:gap-0">
          <Button
            variant={"destructive"}
            onClick={onDelete}
            className="text-white"
          >
            Delete
          </Button>
          <DialogClose asChild>
            <Button type="button" variant="secondary">
              Close
            </Button>
          </DialogClose>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};
