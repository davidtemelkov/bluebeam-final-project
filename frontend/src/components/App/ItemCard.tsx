import { Edit2Icon } from "lucide-react";
import { Button } from "../ui/button";
import { DeleteItem } from "./DeleteItem";

export type ItemCardProps = {
  title: string;
  description?: string;
  image: string;
  customImageHeight?: string;
  onClick?: () => void;
  children?: React.ReactNode;

  onDelete?: () => void;
  onEdit?: () => void;
};

export function ItemCard(props: ItemCardProps) {
  return (
    <div
      onClick={props.onClick}
      className="group relative mb-4 flex flex-col rounded-xl border-2 transition hover:scale-[1.02] hover:border-primary"
    >
      {(!!props.onEdit || !!props.onDelete) && (
        <div className="absolute right-0 hidden scale-75 *:border-[3px] *:transition group-hover:block">
          {!!props.onEdit && (
            <Button
              onClick={props.onEdit}
              variant="outline"
              className="hover:border-primary"
              size="icon"
            >
              <Edit2Icon />
            </Button>
          )}
          {!!props.onDelete && <DeleteItem onDelete={props.onDelete} />}
        </div>
      )}

      <img
        className="w-full rounded-t-[0.6rem] object-cover"
        src={props.image}
        alt={props.title}
        style={{ height: props.customImageHeight ? props.customImageHeight : "128px" }}
      />

      <div className="flex flex-1 flex-col justify-between p-2 px-4">
        <div>
          <h3 className="text-lg font-semibold">{props.title}</h3>
          {props.description && <p className="text-sm">{props.description}</p>}
        </div>
        {props.children}
      </div>
    </div>
  );
}
