import React from "react";

import {
  TooltipProvider,
  Tooltip as TooltipBody,
  TooltipTrigger,
  TooltipContent,
} from "../ui/tooltip";

export const Tooltip: React.FC<{
  target: React.ReactNode;
  description: string;
}> = ({ target, description }) => {
  return (
    <TooltipProvider>
      <TooltipBody>
        <TooltipTrigger>{target}</TooltipTrigger>
        <TooltipContent>
          <p>{description}</p>
        </TooltipContent>
      </TooltipBody>
    </TooltipProvider>
  );
};
