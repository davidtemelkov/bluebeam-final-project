import { Menu } from "lucide-react";
import { useTheme } from "next-themes";
import { useContext, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import { UserContext } from "../Context/UserContext";
import { Button } from "../ui/button";
import { Popover, PopoverContent, PopoverTrigger } from "../ui/popover";
import { ThemeSwitcher } from "./ThemeSwitcher";

export function AppNavBar() {
  const [menuOpen, setMenuOpen] = useState(false);
  const { theme } = useTheme();
  const { isManager } = useContext(UserContext);
  const location = useLocation();

  const shouldDisableLink = (path: string) => {
    // Define the routes where you want to disable the link
    const disabledRoutes = [path];

    // Check if the current location pathname is in the disabledRoutes array
    return disabledRoutes.includes(location.pathname);
  };

  const isActive = (path: string) => {
    return location.pathname === path;
  };

  return (
    <nav className="border-b bg-foreground/10">
      <div className="container sticky flex w-full items-center justify-between py-4">
        <div className="flex lg:flex-1">
          <Link to="/">
            <span className="sr-only">Home Page</span>
            <img
              className="h-16 w-auto rounded-xl opacity-90"
              src={theme === "light" ? "/logo-light.png" : "/logo-dark.png"}
              alt="logo"
            />
          </Link>
        </div>
        <div className="hidden items-center gap-x-4 *:text-lg *:font-semibold sm:flex">
          {!shouldDisableLink("/") && (
            <Link
              to={`/map`}
              className={`hover:bg-foreground/5 ${isActive("/map") ? "text-primary" : ""}`}
            >
              Map
            </Link>
          )}
          {!shouldDisableLink("/") && isManager() && (
            <Link
              to={`/facilities`}
              className={`hover:bg-foreground/5 ${isActive("/facilities") ? "text-primary" : ""}`}
            >
              Facilities
            </Link>
          )}
          {!shouldDisableLink("/") && (
            <Link
              to={`/issues`}
              className={`hover:bg-foreground/5 ${isActive("/issues") ? "text-primary" : ""}`}
            >
              Issues
            </Link>
          )}
          <Link
            to={`/aboutus`}
            className={`hover:bg-foreground/5 ${isActive("/aboutus") ? "text-primary" : ""}`}
          >
            About Us
          </Link>

          <ThemeSwitcher />
        </div>

        <div className="flex sm:hidden">
          <Popover open={menuOpen} onOpenChange={setMenuOpen}>
            <PopoverTrigger asChild>
              <Button variant="outline" size="icon" className="flex sm:hidden">
                <Menu />
              </Button>
            </PopoverTrigger>
            <PopoverContent className="w-36 p-0 py-2 sm:hidden">
              <div className="flex flex-col *:w-full *:px-2 *:py-1 *:text-center *:text-lg *:font-semibold">
                {!shouldDisableLink("/") && (
                  <Link
                    to={`/map`}
                    className={`hover:bg-foreground/5 ${isActive("/map") ? "text-primary" : ""}`}
                    onClick={() => setMenuOpen(false)}
                  >
                    Map
                  </Link>
                )}
                {!shouldDisableLink("/") && isManager() && (
                  <Link
                    to={`/facilities`}
                    className={`hover:bg-foreground/5 ${isActive("/facilities") ? "text-primary" : ""}`}
                    onClick={() => setMenuOpen(false)}
                  >
                    Facilities
                  </Link>
                )}
                {!shouldDisableLink("/") && (
                  <Link
                    to={`/issues`}
                    className={`hover:bg-foreground/5 ${isActive("/issues") ? "text-primary" : ""}`}
                    onClick={() => setMenuOpen(false)}
                  >
                    Issues
                  </Link>
                )}
                <Link
                  to={`/aboutus`}
                  className={`hover:bg-foreground/5 ${isActive("/aboutus") ? "text-primary" : ""}`}
                  onClick={() => setMenuOpen(false)}
                >
                  About Us
                </Link>
              </div>
            </PopoverContent>
          </Popover>

          <ThemeSwitcher />
        </div>
      </div>
    </nav>
  );
}
