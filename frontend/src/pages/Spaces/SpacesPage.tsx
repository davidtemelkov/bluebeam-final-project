import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import { ItemCard } from "@/components/App/ItemCard";
import { SpaceModalPredefinedData } from "@/components/SpaceDialog/ISpaceDialogProps";
import { CreateSpace } from "@/components/SpaceDialog/SpaceDialog";
import Spinner from "@/components/ui/spinner";
import {
  SpaceResponse,
  deleteById,
  getByFacility,
} from "@/services/space.service";
import { getErrorMessage } from "@/utils/getErrorMessage";
import { Link } from "react-router-dom";
import { toast } from "sonner";

export const SpacesPage: React.FC = () => {
  const { facilityParams } = useParams<{ facilityParams: string }>();
  const [spaces, setSpaces] = useState<SpaceResponse[]>([]);
  const [loading, setLoading] = useState(true);

  const [modalPredefinedData, setModalPredefinedData] =
    useState<SpaceModalPredefinedData>();

  const facilityId = facilityParams?.split("~")[0];
  const facilityName = facilityParams?.split("~")[1];

  useEffect(() => {
    getByFacility(facilityId!)
      .then((fetchedSpaces) => setSpaces(fetchedSpaces))
      .catch((e) => toast.error(getErrorMessage(e)))
      .finally(() => setLoading(false));
  }, [facilityId]);

  const addSpace = (newSpaces: SpaceResponse) => {
    setSpaces((prevSpaces) => [...prevSpaces, newSpaces]);
  };

  const handleDelete = async (id: string) => {
    try {
      deleteById(id).catch((e) => toast.error(getErrorMessage(e)));
      setSpaces((prevSpaces) => prevSpaces.filter((space) => space.id !== id));
    } catch (e) {
      toast.error(getErrorMessage(e));
    }
  };

  const updateSpace = (updated: SpaceModalPredefinedData) => {
    setSpaces((prevSpaces) => {
      const idx = prevSpaces.findIndex((space) => space.id === updated.id);
      if (idx == -1) {
        console.error(
          `Local update failed; space with id ${updated.id} not found`,
        );
        toast.error(
          "We successfully updated the space, but it couldn't be updated locally. Please try reloading.",
        );
        return prevSpaces;
      }

      const newSpaces = [...prevSpaces];
      const space = newSpaces[idx];
      newSpaces[idx] = {
        ...space,
        name: updated.name || space.name,
        description: updated.description || space.description,
        blueprintUrl: updated.blueprintUrl || space.blueprintUrl,
      };

      return newSpaces;
    });
  };

  if (loading) {
    return <Spinner />;
  }

  return (
    <div className="container">
      <div className="mb-2 flex w-full items-center justify-between">
        <h2 className="text-xl font-semibold md:text-2xl lg:text-3xl">
          Spaces for {facilityName}
        </h2>
        <CreateSpace
          onClose={() => setModalPredefinedData(undefined)}
          updateSpace={updateSpace}
          predefinedData={modalPredefinedData}
          addSpace={addSpace}
        />
      </div>

      {spaces.length > 0 ? (
        <div className="grid grid-cols-1 gap-x-4 sm:grid-cols-2 lg:grid-cols-3 2xl:grid-cols-4">
          {spaces.map((space) => (
            <ItemCard
              key={space.id}
              title={space.name}
              description={space.description}
              image={space.blueprintUrl}
              onEdit={() =>
                setModalPredefinedData({
                  description: space.description,
                  name: space.name,
                  id: space.id,
                })
              }
              onDelete={() => handleDelete(space.id)}
            >
              <Link
                to={`/${encodeURIComponent(
                  facilityId + "~" + facilityName,
                )}/spaces/${encodeURIComponent(space.id)}`}
                className="text-blue-500"
              >
                View Layout
              </Link>
            </ItemCard>
          ))}
        </div>
      ) : (
        <p className="text-center text-xl font-semibold">No spaces found</p>
      )}
    </div>
  );
};
