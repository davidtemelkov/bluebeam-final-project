import users from "@/data/users.json";

import { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

import { Popover, PopoverContent } from "../../components/ui/popover";
import { PopoverTrigger } from "@radix-ui/react-popover";
import { Image, FilePenIcon, Save, X, PlusIcon } from "lucide-react";
import { IssueMap } from "@/components/IssueMap/IssueMap";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Separator } from "@/components/ui/separator";
import Spinner from "@/components/ui/spinner";
import { FormError } from "@/components/App/FormError";
import { Textarea } from "@/components/ui/textarea";

import {
  IssueLocation,
  IssueResponse,
  UpdateIssueData,
  deleteById,
  getById,
  update,
} from "@/services/issue.service";
import {
  SpaceResponse,
  getById as getSpaceById,
} from "@/services/space.service";
import { toast } from "sonner";

import { STATUS_LABELS } from "@/utils/enums/status";
import { getErrorMessage } from "@/utils/getErrorMessage";
import { capitalizeAllLetters } from "@/utils/string";
import {
  validateDescription,
  validateFile,
  validateName,
} from "@/utils/validations";
import { UserContext } from "@/components/Context/UserContext";
import { DeleteItem } from "@/components/App/DeleteItem";

export const SingleIssueDetails: React.FC = () => {
  const [issue, setIssue] = useState<IssueResponse | null>(null);
  const [space, setSpace] = useState<SpaceResponse | null>(null);
  const { issueId } = useParams<{
    issueId: string;
  }>();
  const [loading, setLoading] = useState(true);
  const [toggleEdit, setToggleEdit] = useState(false);
  const [issueTitle, setIssueTitle] = useState("");
  const [issueDescription, setIssueDescription] = useState("");
  const [issueFile, setIssueFile] = useState<File | null>(null);
  const [isAddingIssue, setIsAddingIssue] = useState(false);
  const [issueCoords, setIssueCoords] = useState<IssueLocation | null>();
  const [editedIssueMarker, setEditedIssueMarker] =
    useState<IssueLocation | null>(null);
  const [errors, setErrors] = useState({
    title: "",
    description: "",
    file: "",
  });
  const { isManager } = useContext(UserContext);
  const navigate = useNavigate();

  useEffect(() => {
    getById(issueId!)
      .then((i) => {
        setIssue(i);
        i && setIssueTitle(i.title);
        i && setIssueDescription(i.description);
      })
      .catch((e) => toast.error(getErrorMessage(e)));
  }, [issueId]);

  useEffect(() => {
    if (issue?.spaceId) {
      getSpaceById(issue.spaceId)
        .then(setSpace)
        .catch((e) => toast.error(getErrorMessage(e)))
        .finally(() => setLoading(false));
    }
  }, [issue]);

  function handleTitle(event: React.FocusEvent<HTMLInputElement>) {
    const error = validateName(event.currentTarget.value);
    setErrors((prevErrors) => ({ ...prevErrors, title: error }));
  }

  function handleDescription(event: React.FocusEvent<HTMLTextAreaElement>) {
    const error = validateDescription(event.currentTarget.value);
    setErrors((prevErrors) => ({ ...prevErrors, description: error }));
  }

  const handleFileChange = (e: React.FocusEvent<HTMLInputElement>) => {
    if (e.target.files) {
      const fileError = validateFile(e.target.files[0], false);
      setErrors((prevErrors) => ({ ...prevErrors, file: fileError }));
      setIssueFile(e.target.files[0]);
    }
  };

  const resetForm = () => {
    issue && setIssueTitle(issue.title);
    issue && setIssueDescription(issue.description);
    setEditedIssueMarker(null);
    setIssueCoords(null);
    setErrors({
      title: "",
      description: "",
      file: "",
    });
  };

  const handleToggle = () => {
    resetForm();
    setToggleEdit(!toggleEdit);
  };

  const formIsValid = () => {
    const titleError = validateName(issueTitle);
    const descriptionError = validateDescription(issueDescription);
    const fileError = validateFile(issueFile, false);
    setErrors({
      title: titleError,
      description: descriptionError,
      file: fileError,
    });

    return !titleError && !descriptionError && !fileError;
  };

  const handleMarkLocation = (event: React.MouseEvent<HTMLDivElement>) => {
    if (isAddingIssue) {
      const rect = event.currentTarget.getBoundingClientRect();
      const coordX = ((event.clientX - rect.left) / rect.width) * 100 - 1.2;
      const coordY = ((event.clientY - rect.top) / rect.height) * 100 - 2;

      const coords: IssueLocation = {
        coordX,
        coordY,
      };

      setEditedIssueMarker(coords);
      setIssueCoords(coords);
      setIsAddingIssue(false);
    }
  };

  const handleSave = () => {
    if (!formIsValid()) {
      toast.error("The form is invalid");
      return;
    }
    if (
      issueTitle == issue?.title &&
      issueDescription == issue?.description &&
      !issueFile &&
      !issueCoords
    ) {
      toast.info("No changes were made.");
      return;
    }

    if (issue) {
      const issueToUpdate: UpdateIssueData = {
        id: issue.id,
      };

      if (issueTitle) {
        issueToUpdate.title = issueTitle;
      }

      if (issueDescription) {
        issueToUpdate.description = issueDescription;
      }

      if (issueFile) {
        issueToUpdate.file = issueFile;
      }

      if (issueCoords) {
        issueToUpdate.coordX = issueCoords.coordX;
        issueToUpdate.coordY = issueCoords.coordY;
      }

      update(issueToUpdate)
        .then((updatedIssue) => {
          setIssue(updatedIssue);
          toast.success("Issue updated successfully!");
        })
        .catch((e: unknown) => toast.error(getErrorMessage(e)));
    }

    handleToggle();
  };

  const handleDelete = async (id: string) => {
    try {
      await deleteById(id);
      navigate("/issues");
    } catch (e) {
      toast.error(getErrorMessage(e));
    }
  };

  const handleStatusChange = async (statusId: string) => {
    if (issue) {
      setIssue({
        ...issue,
        statusId: +statusId,
        resolvedAt:
          +statusId == 2
            ? new Date().toLocaleString("en-GB", {
                dateStyle: "medium",
                timeStyle: "medium",
              })
            : null,
      });

      await update({
        id: issue.id,
        statusId: +statusId,
        resolvedAt:
          +statusId == 2
            ? new Date().toLocaleString("en-GB", {
                dateStyle: "medium",
                timeStyle: "medium",
              })
            : null,
      }).catch((e) => toast.error(getErrorMessage(e)));
    }
  };

  const handleAssignToNoOne = async () => {
    if (issue) {
      setIssue({ ...issue, assigneeId: null });

      await update({
        id: issue.id,
        assigneeId: undefined,
      }).catch((e) =>
        console.error("Error creating space", getErrorMessage(e)),
      );
    }
  };

  const handleAssigneeChange = async (assigneeId: string) => {
    if (issue) {
      if (assigneeId === "None") {
        await handleAssignToNoOne();
      } else {
        setIssue({ ...issue, assigneeId: +assigneeId });

        await update({
          id: issue.id,
          assigneeId: +assigneeId,
        }).catch((e) => toast.error(getErrorMessage(e)));
      }
    }
  };

  if (loading) {
    return <Spinner />;
  }

  return (
    <div className="container">
      <div className="mb-2 flex w-full items-center justify-between">
        {!toggleEdit ? (
          <h3 className="text-lg font-bold md:text-3xl">{issue?.title}</h3>
        ) : (
          <div className="flex w-1/3 flex-col">
            <Label className="mb-2 font-semibold">Title:</Label>
            <Input
              value={issueTitle}
              onBlur={handleTitle}
              onChange={(e) => setIssueTitle(e.target.value)}
            />
            <FormError error={errors.title} />
          </div>
        )}
        <div className="flex items-center gap-x-4 pt-1">
          <div className="flex flex-col text-right">
            <p className="text-xs opacity-90 md:text-sm">
              {issue?.createdAt &&
                `Created At: ${new Date(issue.createdAt).toLocaleString(
                  "en-GB",
                  {
                    dateStyle: "short",
                    timeStyle: "short",
                  },
                )}`}
            </p>
            <p className="text-xs opacity-75">
              {issue?.resolvedAt &&
              new Date(issue.resolvedAt).getFullYear() > 1000
                ? `Resolved At: ${new Date(issue.resolvedAt).toLocaleString(
                    "en-GB",
                    {
                      dateStyle: "short",
                      timeStyle: "short",
                    },
                  )}`
                : "Unresolved"}
            </p>
          </div>
        </div>
      </div>
      <Separator className="my-4" />

      <div className="flex gap-4">
        <div className="flex flex-col gap-2">
          <p className="font-semibold">Status:</p>
          <Select
            value={String(issue?.statusId)}
            onValueChange={handleStatusChange}
          >
            <SelectTrigger className="h-[30px] w-[140px] md:w-[170px]">
              <SelectValue placeholder="Select a status" />
            </SelectTrigger>
            <SelectContent>
              {STATUS_LABELS.map(capitalizeAllLetters).map(
                (status, statusIdx) => (
                  <SelectItem value={String(statusIdx)} key={statusIdx}>
                    {status}
                  </SelectItem>
                ),
              )}
            </SelectContent>
          </Select>
        </div>
        <div className="flex flex-col gap-2">
          <p className="font-semibold">Assignee:</p>
          <Select
            value={
              issue?.assigneeId?.toString()
                ? issue.assigneeId.toString()
                : "None"
            }
            onValueChange={handleAssigneeChange}
          >
            <SelectTrigger className="h-[30px] w-[140px] md:w-[170px]">
              <SelectValue placeholder="Select an assignee" />
            </SelectTrigger>
            <SelectContent>
              <SelectItem value={"None"} key={"None"}>
                None
              </SelectItem>
              {users.map((user) => (
                <SelectItem value={user.id.toString()} key={user.id}>
                  {user.name}
                </SelectItem>
              ))}
            </SelectContent>
          </Select>
        </div>
      </div>

      {!toggleEdit ? (
        <>
          <div className="my-4 flex flex-col gap-2">
            <p className="font-semibold">Description:</p>
            <p className="text-pretty">{issue?.description.toString()}</p>
          </div>
        </>
      ) : (
        <div className="mb-4 mt-2">
          <Label className="mt-2 font-semibold">Description:</Label>
          <Textarea
            className="my-2"
            value={issueDescription}
            onBlur={handleDescription}
            onChange={(e) => setIssueDescription(e.target.value)}
          />
          <FormError error={errors.description} />
        </div>
      )}

      {!toggleEdit ? (
        issue?.photoUrl && (
          <Popover>
            <PopoverTrigger className="my-4 flex flex-row items-center gap-1 text-base text-blue-500">
              <Image size={20} /> View Image
            </PopoverTrigger>
            <PopoverContent>
              <img src={issue?.photoUrl} alt={"photo-of-issue"} />
            </PopoverContent>
          </Popover>
        )
      ) : (
        <div className="mb-4 w-11/12 md:w-1/3">
          <Label className="mt-2 font-semibold">Issue Image:</Label>
          <Input
            type="file"
            className="mt-2 file:text-secondary-foreground"
            onChange={handleFileChange}
          />
          <FormError error={errors.file} />
        </div>
      )}

      {isManager() && (
        <div className="mb-4">
          {toggleEdit && (
            <Button
              onClick={handleSave}
              variant="outline"
              title="Save changes"
              size="icon"
              className="hover:bg-green-200 hover:text-black"
            >
              <Save />
            </Button>
          )}
          <Button
            onClick={handleToggle}
            variant="outline"
            title="Cancel"
            size="icon"
            className={`${toggleEdit ? "hover:bg-red-200 hover:text-black" : "hover:border-primary"}`}
          >
            {toggleEdit ? <X /> : <FilePenIcon />}
          </Button>
          {toggleEdit && (
            <Button
              variant="outline"
              size="icon"
              title="Move issue location"
              className={`${isAddingIssue ? `bg-primary/20` : ""} hover:border-primary`}
              onClick={() => setIsAddingIssue(!isAddingIssue)}
            >
              <PlusIcon />
            </Button>
          )}
          <DeleteItem onDelete={() => handleDelete(issue!.id.toString())} />
        </div>
      )}

      {issue && space && (
        <IssueMap
          issues={[issue]}
          blueprintUrl={space.blueprintUrl}
          onAdd={handleMarkLocation}
          isAddingIssue={isAddingIssue}
          newIssue={editedIssueMarker}
        />
      )}
    </div>
  );
};

export default SingleIssueDetails;
