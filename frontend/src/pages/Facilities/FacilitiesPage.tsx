import { useEffect, useState } from "react";

import { Link } from "react-router-dom";

import { CreateFacility } from "@/components/FacilityDialog/FacilityDialog";

import { ItemCard } from "@/components/App/ItemCard";
import Spinner from "@/components/ui/spinner";

import { FacilityModalPredefinedData } from "@/components/FacilityDialog/IFacilityDialogProps";
import {
  FacilityResponse,
  deleteById,
  getAll,
} from "@/services/facility.service";
import { getErrorMessage } from "@/utils/getErrorMessage";
import { toast } from "sonner";

export const FacilitiesPage: React.FC = () => {
  const [facilities, setFacilities] = useState<FacilityResponse[]>([]);
  const [loading, setLoading] = useState(true);

  const [modalPredefinedData, setModalPredefinedData] =
    useState<FacilityModalPredefinedData>();

  useEffect(() => {
    getAll()
      .then((fetchedFacilities) => setFacilities(fetchedFacilities))
      .catch((e) => toast.error(getErrorMessage(e)))
      .finally(() => setLoading(false));
  }, []);

  const addFacility = (newFacility: FacilityResponse) => {
    setFacilities((prevFacilities) => [...prevFacilities, newFacility]);
  };

  const handleDelete = async (id: string) => {
    try {
      deleteById(id).catch((e) => toast.error(getErrorMessage(e)));
      setFacilities((prevFacilities) =>
        prevFacilities.filter((facility) => facility.id !== id),
      );
    } catch (e) {
      toast.error(getErrorMessage(e));
    }
  };

  const updateFacility = (updated: FacilityModalPredefinedData) => {
    setFacilities((prevFacilities) => {
      const idx = prevFacilities.findIndex(
        (facility) => facility.id === updated.id,
      );
      if (idx == -1) {
        console.error(
          `Local update failed; facility with id ${updated.id} not found`,
        );
        toast.error(
          "We successfully updated the facility, but it couldn't be updated locally. Please try reloading.",
        );
        return prevFacilities;
      }

      const newFacilities = [...prevFacilities];
      const facility = newFacilities[idx];
      newFacilities[idx] = {
        ...facility,
        name: updated.name || facility.name,
        address: updated.address || facility.address,
        photoUrl: updated.photoUrl || facility.photoUrl,
      };

      return newFacilities;
    });
  };

  if (loading) {
    return <Spinner />;
  }

  return (
    <div className="container">
      <div className="mb-2 flex w-full items-center justify-between">
        <h2 className="text-3xl font-semibold">Facilities</h2>
        <CreateFacility
          onClose={() => setModalPredefinedData(undefined)}
          addFacility={addFacility}
          updateFacility={updateFacility}
          predefinedData={modalPredefinedData}
        />
      </div>

      {facilities.length > 0 ? (
        <div className="grid grid-cols-1 gap-x-4 sm:grid-cols-2 lg:grid-cols-3 2xl:grid-cols-4">
          {facilities.map(({ id, address, name, photoUrl }) => (
            <ItemCard
              key={id}
              title={name}
              description={address}
              image={photoUrl}
              onEdit={() => setModalPredefinedData({ address, name, id })}
              onDelete={() => handleDelete(id)}
            >
              <Link
                to={`/${encodeURIComponent(id + "~" + name)}/spaces`}
                className="text-blue-500"
              >
                View Spaces
              </Link>
            </ItemCard>
          ))}
        </div>
      ) : (
        <p className="text-center text-xl font-semibold">No facilities found</p>
      )}
    </div>
  );
};
