import { Link } from "react-router-dom";
import { ImageIcon, User2Icon } from "lucide-react";
import { cn } from "@/lib/utils";
import {
  statusBackgroundColors,
  statusBorderColors,
  statusHoverBorderColors,
} from "@/utils/colors";
import { IssueResponse } from "@/services/issue.service";
import { useState } from "react";

export function IssueCard({
  issue,
  handleView,
}: {
  issue: IssueResponse;
  handleView: (issue: IssueResponse) => void;
}) {
  const [moving, setMoving] = useState<boolean>(false);

  const handleDragStart = (e: React.DragEvent<HTMLDivElement>) => {
    e.dataTransfer.setData("issueId", issue.id.toString());
    setMoving(true);
  };

  return (
    <div
      draggable
      onDragStart={handleDragStart}
      onDragEnd={() => setMoving(false)}
      onClick={() => handleView(issue)}
      className={cn(
        "group relative mx-1 mb-4 min-w-[30px] rounded-xl border-2 transition duration-150 hover:scale-[1.01] hover:cursor-move",
        statusBorderColors[issue.statusId],
        statusHoverBorderColors[issue.statusId],
        statusBackgroundColors[issue.statusId],
        moving ? "border-dashed" : "",
      )}
    >
      <div className="p-2 px-4">
        <div className="flex justify-between">
          <h3 className="text-lg font-semibold hover:cursor-pointer">
            <Link to={`/issues/${issue.id}`}>{issue.title}</Link>
          </h3>

          <div className="flex items-center gap-2 *:h-5 *:w-5 *:text-slate-600/50 *:transition-colors  dark:*:text-slate-100/75">
            {issue.photoUrl != null && <ImageIcon />}
            {issue.assigneeId != null && <User2Icon />}
          </div>
        </div>
        {!!issue.facility && !!issue.space && (
          <div className="flex items-center gap-1 text-xs">
            <span>{issue.facility.name}</span>
            <div className="h-1 w-1 rounded-full bg-foreground/40" />
            <span>{issue.space.name}</span>
          </div>
        )}

        <p className="max-h-[3ch] w-full truncate py-1 text-sm text-muted-foreground">
          {issue.description}
        </p>
      </div>
    </div>
  );
}
