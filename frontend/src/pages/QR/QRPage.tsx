export const QRPage: React.FC = () => {
  return (
    <div className="container grid place-items-center">
      <div className="flex h-fit w-fit scale-125 flex-col items-center rounded-3xl p-8 *:pointer-events-none *:select-none dark:bg-gray-200">
        <img className="bg-transparent accent-red-400" src="/solaris-qr.svg" />
        <h4 className="text-xl font-semibold text-black">
          https://solaris-bb.netlify.app/
        </h4>
      </div>
    </div>
  );
};
