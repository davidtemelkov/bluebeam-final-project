export const ROLE_LABELS = Object.freeze(["Facility Manager", "Worker"]);

// note: these indexes are also used in the backend
export enum Role {
  MANAGER = 0,
  WORKER = 1,
}

export function getRoleLabel(role: Role) {
  return ROLE_LABELS[role];
}
