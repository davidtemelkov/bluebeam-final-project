/* eslint-disable @typescript-eslint/no-unused-vars */
// TODO: remove eslint-disable

export type FacilityBase = {
  id: string;
  name: string;
  address: string;

  lng: number;
  lat: number;
};

export type FacilityResponse = FacilityBase & {
  photoUrl: string;
  issuesCount?: number;
};

export type FacilityBody = FacilityBase & {
  file: File;
};

export type UpdateFacilityData = { id: string } & Partial<{
  name: string;
  address: string;
  file: File;
  lng: number;
  lat: number;
}>;

export interface IFacilityService {
  getAll(): Promise<FacilityResponse[]>;
  getAllWithIssuesCount(): Promise<FacilityResponse[]>;
  getById(id: number | string): Promise<FacilityResponse | null>;
  deleteById(id: number | string): Promise<void>;
  update(data: UpdateFacilityData): Promise<FacilityResponse>;
  create(facility: Omit<FacilityBody, "id">): Promise<FacilityResponse>;
}

const backendUrl = typeof import.meta.env.VITE_BACKEND_URL === "string" ? import.meta.env.VITE_BACKEND_URL : "http://localhost:8081";
const endpoint = backendUrl + "/api/v1/facility";

const facilityService: IFacilityService = {
  getAll: async function () {
    const response = await fetch(`${endpoint}/all`);

    if (!response.ok) {
      throw new Error("Failed to fetch facilities");
    }

    // 204 means no content
    if (response.status === 204) {
      return [];
    }

    const facilities = await response.json();

    return facilities;
  },
  getAllWithIssuesCount: async function () {
    const response = await fetch(`${endpoint}/all/count`);

    if (!response.ok) {
      throw new Error("Failed to fetch facilities");
    }

    // 204 means no content
    if (response.status === 204) {
      return [];
    }

    const facilities = await response.json();

    return facilities;
  },
  getById: async function () {
    throw new Error("Function not implemented.");
  },
  deleteById: async function (id) {
    const response = await fetch(`${endpoint}/${id}`, {
      method: "DELETE",
    });

    if (!response.ok) {
      throw new Error("Failed to delete facility");
    }
  },
  update: async function ({ id, address, file, name, lat, lng }) {
    const formData = new FormData();
    if (name) formData.append("name", name);
    if (address) formData.append("address", address);
    if (file) formData.append("file", file);
    if (lat) formData.append("lat", lat.toString());
    if (lng) formData.append("lng", lng.toString());

    const response = await fetch(`${endpoint}/${id}`, {
      method: "PUT",
      body: formData,
    });

    if (!response.ok) {
      throw new Error("Failed to update facility");
    }

    return response.json();
  },
  create: async function (facility) {
    const formData = new FormData();
    formData.append("name", facility.name);
    formData.append("address", facility.address);
    formData.append("lng", facility.lng.toString());
    formData.append("lat", facility.lat.toString());
    formData.append("file", facility.file);

    const response = await fetch(endpoint, {
      method: "POST",
      body: formData,
    });

    if (!response.ok) {
      throw new Error("Failed to create facility");
    }

    const createdFacility = await response.json();

    return createdFacility;
  },
};

export const {
  getAll,
  getAllWithIssuesCount,
  getById,
  deleteById,
  update,
  create,
} = facilityService;
