using System.ComponentModel.DataAnnotations;

namespace backend.BodyDtos;
public class CreateIssueDto
{
    [StringLength(50, MinimumLength = 2, ErrorMessage = "The title field must be between 2 and 50 characters.")]
    public string? title { get; set; }

    [StringLength(400, MinimumLength = 2, ErrorMessage = "The description field must be between 2 and 50 characters.")]
    public string? description { get; set; }

    [Range(0.00001, int.MaxValue, ErrorMessage = "The status id must be a positive number.")]
    public double? coordX { get; set; }

    [Range(0.00001, int.MaxValue, ErrorMessage = "The status id must be a positive number.")]
    public double? coordY { get; set; }

    [Range(0, 2, ErrorMessage = "The status id must be 0, 1 or 2.")]
    public int? statusId { get; set; }

    [Range(1, int.MaxValue, ErrorMessage = "The assignee id must be a positive number.")]
    public int? assigneeId { get; set; }

    public DateTime? resolvedAt { get; set; }

    public IFormFile? file { get; set; }
}
