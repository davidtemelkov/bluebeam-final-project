using System.ComponentModel.DataAnnotations;

namespace backend.BodyDtos;
public class CreateFacilityDto
{
    [StringLength(30, MinimumLength = 2, ErrorMessage = "The name field must be between 2 and 50 characters.")]
    public string? name { get; set; }

    [StringLength(50, MinimumLength = 2, ErrorMessage = "The address field must be between 2 and 50 characters.")]
    public string? address { get; set; }

    [Range(-90, 90, ErrorMessage = "The latitude must be between -90 and 90.")]
    public double? lat { get; set; }

    [Range(-180, 180, ErrorMessage = "The longitude must be between -180 and 180.")]
    public double? lng { get; set; }

    public IFormFile? file { get; set; }
}
