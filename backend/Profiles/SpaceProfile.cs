using AutoMapper;
using backend.Dtos;
using backend.Models;

namespace backend.Profiles.SpaceProfile;

public class SpaceProfile : Profile
{
  public SpaceProfile()
  {
    CreateMap<Space, SpaceDto>().ReverseMap();
  }
}

