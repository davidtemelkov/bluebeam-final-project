using backend.Models;
using backend.Dtos;
using AutoMapper;
using backend.BodyDtos;

namespace backend.Profiles;

public class FacilityProfile : Profile
{
    public FacilityProfile()
    {
        CreateMap<Facility, FacilityDto>().ReverseMap();
        CreateMap<BodyFacilityDto, FacilityDto>().ReverseMap();
    }
}

