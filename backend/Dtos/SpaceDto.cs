using System.ComponentModel.DataAnnotations;

namespace backend.Dtos;
public class SpaceDto
{
    public int id { get; set; }

    [Required(ErrorMessage = "The name field is required")]
    public required string name { get; set; }

    [Required(ErrorMessage = "The description field is required")]
    public required string description { get; set; }

    [Required(ErrorMessage = "The blueprintUrl field is required")]
    public required string blueprintUrl { get; set; }

    public required int facilityId { get; set; }
}
