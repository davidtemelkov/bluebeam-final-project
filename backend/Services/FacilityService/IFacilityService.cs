﻿using backend.BodyDtos;
using backend.Dtos;
using backend.Models;
using CloudinaryDotNet.Actions;

namespace backend.Services.FacilityService
{
    public interface IFacilityService
    {
        Task<List<FacilityDto>> GetAllFacilitiesAsync();

        Task<List<FacilityWithCountDto>> GetAllFacilitiesWithIssueCountAsync();

        Task<FacilityDto> GetFacilityByIdAsync(int id);

        Task<FacilityDto> AddFacility(BodyFacilityDto facilityFromForm);

        Task<FacilityDto> UpdateFacility(int id, CreateFacilityDto createBody);

        Task<bool> DeleteFacility(int id);
    }
}