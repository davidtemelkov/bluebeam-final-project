﻿using backend.BodyDtos;
using backend.Models;
using CloudinaryDotNet.Actions;

namespace backend.Services.ImageService
{
    public interface IImageService
    {
        Task<ImageUploadResult?> UploadAsync(IFormFile file);
        void ValidateImage(IFormFile file);
        Task<Facility> MapToFacilityWithPhotoUrl(BodyFacilityDto bodyFacilityDto);
        Task<Issue> MapToIssueWithPhotoUrl(BodyIssueDto bodyIssueDto);
        Task<Space> MapToSpaceWithPhotoUrl(BodySpaceDto bodySpaceDto);
    }
}
